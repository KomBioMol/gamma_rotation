# Gamma Rotation

A small package to compute angle of gamma rotation in ATP synthase (and similar cases) using optimal rotation matrix.

## Algorithm

The package is utilizing [Kabsch algorithm](https://en.wikipedia.org/wiki/Kabsch_algorithm) to compute angle between given structures and reference one (default first frame). It allows you to prefit structure on group of atoms from provided index file. There is also option to precenter structure. Two ways of defining rotation axis are possible: constant vector (eg. Oz axis versor) or principal axis of inertia of given group (corresponding with maximal or minimal moment of inertia). You can also slice structure along the axis to compute rotation angle for each of the slices.

## Dependencies

The package is dedicated for [GROMACS](http://www.gromacs.org/) users, consequently operates on its file formats (i.e. `xtc` trajectory files, `pdb` structure files and `ndx` index files). To perform basic trajectory operations [MDTraj package](http://mdtraj.org/1.9.3/) is used and for mathematic operations [numpy](https://numpy.org/) and [scipy](https://www.scipy.org/) are used.

## Installation

### Creating conda environment (optional)

The conda environments are IMO convenient, because you do not interfere with system python and are able to create one with all dependencies with just one command! Do the following:

    git clone https://gitlab.com/KomBioMol/gamma_rotation.git
    cd gamma_rotation
    conda env create -f environment.yml 
    conda activate gamma_rotation

Then proceed with installation the package:

    pip install .

### Pip

You can skip previous step and just install requirements and the package with pip:

    git clone https://gitlab.com/KomBioMol/gamma_rotation.git
    cd gamma_rotation
    pip install -r requirements.txt
    pip install .

## Usage

Once installed you have access to script `gamma_rotation`. Feel free to run `gamma_rotation -h` to see all options.

## Licence

The MIT License (MIT)

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND!

## Contribution

We would appreciate any comments, advises and help with developing and maintaining this package.
