from optparse import OptionParser


__version__ = "0.1"
description = "A small package to compute angle of gamma rotation in ATP synthase (and similar cases) using optimal rotation matrix."


def parse():
    """Returns parser options.
    """
    parser = OptionParser(
            description=description,
            version=f"%prog {__version__}"
            )
    parser.add_option(
            "-v",
            "--verbose",
            dest="verbose",
            action="store_true",
            help="be noisy")
    parser.add_option(
            "-f",
            "--traj",
            dest="traj",
            action="store",
            type="string",
            help="trajectory [xtc, trr etc.]")
    parser.add_option(
            "-s",
            "--struct",
            dest="struct",
            action="store",
            type="string",
            help="structure [pdb, gro etc.]")
    parser.add_option(
            "-n",
            "--ndx",
            dest="ndx",
            action="store",
            type="string",
            help="index [ndx]")
    parser.add_option(
            "-l",
            "--slices",
            dest="slices",
            action="store",
            default=1,
            type="int",
            help="number of slices")
    parser.add_option(
            "-c",
            "--center",
            dest="center",
            action="store",
            type="string",
            help="group for precentering")
    parser.add_option(
            "-t",
            "--fit",
            dest="fit",
            action="store",
            type="string",
            help="group for prefitting")
    parser.add_option(
            "-a",
            "--axis",
            dest="axis",
            action="store",
            type="int",
            nargs=3,
            help="rotation axis coordinates")
    parser.add_option(
            "-m",
            "--moment",
            dest="moment",
            action="store",
            type="string",
            help="Group chosen for computing axis based on moment of inertia")
    parser.add_option(
            "-x",
            "--extrem",
            dest="extrem",
            action="store",
            choices=("min", "max"),
            help="choice of principal axis (min, mid, max of corresponding moment of inertia)")
    parser.add_option(
            "-r",
            "--ref",
            dest="ref",
            action="store",
            type="string",
            help="reference structure, default: first frame")
    parser.add_option(
            "-u",
            "--unit",
            dest="unit",
            action="store",
            default="rad",
            choices=("rad", "deg"),
            help="angle unit: rad or deg, default: rad")
    parser.add_option(
            "-g",
            "--group",
            dest="group",
            action="store",
            type="string",
            help="Group chosen for rotation calculation")
    parser.add_option(
            "-o",
            "--out",
            dest="out",
            action="store",
            default="rotation.dat",
            type="string",
            help="Name of output file")
    opt, args = parser.parse_args()
    if not opt.ndx:
        parser.error("Index file required!")
    if not opt.traj:
        parser.error("Trajectory required!")
    if not opt.struct:
        parser.error("Structure required!")
    # if (not opt.axis and opt.center) or (opt.axis and not opt.center):
        # parser.error("Options -a and -c are both required!")
    if (not opt.moment and opt.extrem) or (opt.moment and not opt.extrem):
        parser.error("Options -m and -x are both required!")
    if (opt.axis or opt.center) and (opt.moment or opt.extrem):
        parser.error("Option pairs -a/-c and -m/-x are mutually exclusive!")
    if opt.center and opt.moment:
        parser.error("Options -c and -m are mutually exclusive!")
    if opt.fit == opt.group:
        parser.error("The same group in options -t and -g does not make sens!")
    return opt


class Noise():

    def __init__(self, verbose):
        self.verbose = verbose

    def __call__(self, com, **kwargs):
        if self.verbose:
            print(com, **kwargs)
        else:
            pass
