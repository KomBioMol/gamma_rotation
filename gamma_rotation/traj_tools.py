import mdtraj as md
import numpy as np
from .optimal_rotation import *
from .ndx import Ndx


def find_marginal_atoms(ref, axis):
    """Finding marginal atoms projected on given axis

    Parameters
    ----------
    ref : mdtraj.Trajectory
        trajectory
    axis : np.ndarray of shape (3,)
        rotation axis

    Returns
    -------
    tuple of two int
        marginal atom indices

    """
    assert len(ref) == 1
    assert len(axis) == 3
    xyz = ref.xyz[0]
    proj = np.einsum("ij,j->i", xyz, axis)

    return np.argmin(proj), np.argmax(proj)


def slice_along_axis(ref, axis, n=1):
    """Dividing atoms on groups based on position of their projections on
    given axis

    Parameters
    ----------
    ref : mdtraj.Trajectory
        trajectory
    axis : np.ndarray of shape (3,)
        rotation axis
    n : int, optional
        number of slices

    Returns
    -------
    Ndx
        index object with goups containing slices

    """
    assert len(ref) == 1
    assert len(axis) == 3
    xyz = ref.xyz[0]
    proj = np.einsum("ij,j->i", xyz, axis)

    # adding here epsilon allows the max value to be dropped into the last bin
    edges = np.linspace(
            np.min(proj),
            np.max(proj) + 100*np.finfo(float).eps,
            n + 1)
    inds = np.digitize(proj, edges)
    # creating new ndx with slices
    ndx = Ndx()
    for i in range(n):
        ndx[f"slice_{i+1}"] = np.where(inds == i + 1)[0]
    return ndx


def find_axis(traj, moment='min', margins=None):
    """Use md.compute_inertia_tensor to find main axis of inertia
        corresponding to min (max) eigenvalue

    Parameters
    ----------
    traj : mdtraj.Trajectory of length t
        trajectory
    moment : string ('min, 'max'), optional
        moment of inertia corresponding to computing axis
    margins : tuple of two int
        marginal atoms

    Returns
    -------
    np.ndarray of shape (t,3)
        array with coordinates of axis for each frame

    """
    inertia_tensor = md.compute_inertia_tensor(traj)
    vals, vecs = np.linalg.eig(inertia_tensor)
    if moment == 'min':
        ind = np.argmin(vals, axis=1)
    elif moment == 'max':
        ind = np.argmax(vals, axis=1)
    result = np.array([vecs[i, :, ind[i]] for i in range(len(ind))])
    if margins is not None:
        assert len(margins) == 2, "Expected exactly TWO marginal atoms!"
        ref_axis = traj.xyz[:, margins[1], :] - traj.xyz[:, margins[0], :]
        proj = np.einsum('ij,ij->i', ref_axis, result)
        result = np.where(proj >= 0, result, -1*result)
    return result


def compute_optimal_rotation(traj, axis, point, ref=None, deg=False):
    """Computing optimal rotation
    Parameters
    ----------
    traj : mdtraj.Trajectory
        trajectory of length T
    axis : np.ndarray of shape (3,)
        axis of rotation
    ref : mdtraj.Trajectory
        reference structure; first frame if not stated
    deg : bool, optional
        determines if angle is in degrees, default: radians

    Returns
    -------
    optimal_angle : np.ndarray of shape (T,)
        optimal angle of rotation for all frames

    """
    if not ref:
        ref = traj[0]
    result = []
    for frame in traj:
        xyz = align_with_Oz(frame.xyz[0], point, axis)
        refxyz = align_with_Oz(ref.xyz[0], point, axis)
        result.append(compute_optimal_angle_Oz(xyz, refxyz, deg=deg))

    return np.array(result)
