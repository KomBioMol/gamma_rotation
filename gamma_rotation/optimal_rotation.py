import numpy as np
import scipy.optimize as opt


def rotate(xyz, angle, axis=np.array([0, 0, 1]), deg=False):
    """Implemented this:
    https://en.wikipedia.org/wiki/Rotation_matrix#Rotation_matrix_from_axis_and_angle
    assuming rotation around axis going through origin

    Parameters
    ----------
    xyz : np.ndarray of shape (N, 3)
        cartesian coordinates of N atoms
    angle : float
        rotation angle
    axis : np.ndarray of shape (3,)
        rotation axis
    deg : bool, optional
        determines if angle is in degrees, default: radians

    Returns
    -------
    np.ndarray of shape (N, 3)
        coordinates of rotated system

    """
    if deg:
        angle = angle * np.pi / 180
    if np.linalg.norm(axis) != 1:
        axis = axis / np.linalg.norm(axis)
    R = [
            [
                np.cos(angle)+axis[0]**2*(1-np.cos(angle)),
                axis[0]*axis[1]*(1-np.cos(angle))-axis[2]*np.sin(angle),
                axis[0]*axis[2]*(1-np.cos(angle))-axis[1]*np.sin(angle)
            ],
            [
                axis[1]*axis[0]*(1-np.cos(angle))-axis[2]*np.sin(angle),
                np.cos(angle)+axis[1]**2*(1-np.cos(angle)),
                axis[1]*axis[2]*(1-np.cos(angle))-axis[0]*np.sin(angle)
            ],
            [
                axis[2]*axis[0]*(1-np.cos(angle))-axis[1]*np.sin(angle),
                axis[2]*axis[1]*(1-np.cos(angle))-axis[0]*np.sin(angle),
                np.cos(angle)+axis[2]**2*(1-np.cos(angle))
            ]
    ]
    return np.dot(xyz, R)


def translate(xyz, vec=None, group=None, mass=None):
    """Translate by vec or, if not given, move CoG (of group or all) to (0,0,0)
    TODO: add mass weights
    TODO: add group choice

    Parameters
    ----------
    xyz : np.ndarray of shape (N, 3)
        cartesian coordinates of N atoms
    vec : np.ndarray of shape (3,), optional
        translation vector
    group : np.ndarray of shape (M,), optional
        group of M<N atoms to compute CoG
    mass : np.ndarray of shape (N,) or (M,), optional
        when provided, computes CoM instead of CoG of N atoms
        (or M when group provided)

    Returns
    -------
    np.ndarray of shape (N, 3)
        coordinates of rotated system

    """
    if vec is None:
        CoG = np.mean(xyz, axis=0)
        vec = -1 * CoG
    else:
        pass

    return xyz + vec[np.newaxis, :]


def rottrans(xyz, angle, point, vec):
    """Rotate around axis with vec direction and going through point

    Parameters
    ----------
    xyz : TODO
    angle: TODO
    point : TODO
    vec : TODO

    Returns
    -------
    TODO

    """
    pass


def align_with_Oz(xyz, point, vec):
    """Translate and rotate coordinates to align point with (0,0,0) and vec
    with z axis

    Parameters
    ----------
    xyz : np.ndarray of shape (N, 3)
        cartesian coordinates of N atoms
    point: np.ndarray of shape (3,)
        point to align with origin
    vec : np.ndarray of shape (3,)
        vector to aligh with z axis

    Returns
    -------
    np.ndarray of shape (N, 3)
        coordinates of aligned system

    """
    vec = vec / np.linalg.norm(vec)
    xyz = translate(xyz, vec=-1*point)
    ang = np.arccos(np.dot(vec, np.array([0, 0, 1])))
    axis = -1 * np.cross(vec, np.array([0, 0, 1]))
    if ang == 0 and np.allclose(axis, 0):
        return xyz
    else:
        return rotate(xyz, ang, axis)


def compute_optimal_angle_Oz(xyz, ref, deg=False):
    """Compute optimal rotation angle to reference arount z axis

    Parameters
    ----------
    xyz : np.ndarray of shape (N, 3)
        cartesian coordinates of N atoms
    ref: np.ndarray of shape (N, 3)
        cartesian coordinates of reference
    deg : bool, optional
        determines if angle is in degrees, default: radians

    Returns
    -------
    float
        optimal angle

    """
    ref = ref.T
    xyz = xyz.T

    H = ref.dot(np.transpose(xyz))
    U, s, Vh = np.linalg.svd(H, full_matrices=False)

    d = np.linalg.det(Vh.T.dot(U.T))
    d_matrix = np.diag(np.array([1, 1, d]))
    R = Vh.T.dot(d_matrix).dot(U.T)
    sinA = R[1, 0]
    cosA = R[0, 0]

    if sinA >= 0 and cosA >= 0:
        angle = np.arcsin(sinA)
    elif sinA >= 0 and cosA < 0:
        angle = np.pi - np.arcsin(sinA)
    elif sinA < 0 and cosA < 0:
        angle = -np.pi + np.arcsin(np.abs(sinA))
    elif sinA < 0 and cosA >= 0:
        angle = - np.arcsin(np.abs(sinA))

    if deg:
        angle = angle*180/np.pi

    return angle


def minimize_rmsd(xyz, ref, axis=np.array([0, 0, 1]), deg=False):
    """Testing function which minimizes RMSD numerically

    Parameters
    ----------
    xyz : np.ndarray of shape (N, 3)
        cartesian coordinates of N atoms
    ref: np.ndarray of shape (N, 3)
        cartesian coordinates of reference
    deg : bool, optional
        determines if angle is in degrees, default: radians

    Returns
    -------
    float
        optimal angle
    xyz : TODO
    ref : TODO
    deg : TODO, optional

    """
    def f(x):
        return np.sqrt(np.mean(np.power(rotate(xyz, -1*x[0]) - ref, 2)))
    res = opt.minimize(f, x0=[0])
    return res.x[0]
