import mdtraj as md
import numpy as np

from gamma_rotation.traj_tools import (
        find_marginal_atoms,
        slice_along_axis,
        find_axis,
        compute_optimal_rotation
    )
from gamma_rotation.ndx import Ndx
from gamma_rotation.utils import parse, Noise


def main():
    # parsing options
    opt = parse()

    noise = Noise(opt.verbose)

    noise(f"Group chosen to compute rotation: {opt.group}")

    deg = opt.unit == "deg"
    noise(f"Unit: {opt.unit}")

    # loading index
    ndx = Ndx(opt.ndx)
    noise(f"File {opt.ndx} loaded successfully")

    # loading reference frame
    if not opt.ref:
        reference = md.load_frame(opt.traj, 0, top=opt.struct)
        noise("First frame is chosen as reference")
    else:
        reference = md.load(opt.ref)
        noise(f"{opt.ref} is chosen as reference")

    if opt.moment and opt.extrem:
        ref = reference.atom_slice(ndx[opt.moment])
        axis = find_axis(ref, moment=opt.extrem)[0]
        margins = find_marginal_atoms(ref, axis)
    else:
        axis = opt.axis

    ref = reference.atom_slice(ndx[opt.group])
    grps = slice_along_axis(ref, axis, opt.slices)
    noise(f"Structure slicet to {opt.slices} slices")
    if opt.slices > 1:
        grps.writefile("slices_along_axis.ndx")

    # loading trajectory in chunks
    chunk = 1000
    noise(f"Loading trajectory in {chunk}-frame chunks...\n")
    trajs = md.iterload(opt.traj, top=opt.struct, chunk=chunk)

    result = []

    for i, traj in enumerate(trajs):
        noise(f"\033[AProceeding with chunk no {i + 1}...\n")
        if opt.fit:
            noise(f"\033[APrefitting chunk no {i + 1}...\n")
            traj.superpose(reference, atom_indices=ndx[opt.fit])
        for j, frame in enumerate(traj):
            noise(
                f"\033[Aanalyzing frame no {j + 1} from chunk no {i+1}...",
                end=" ")
            if opt.axis and opt.center:
                com = md.compute_center_of_mass(
                        frame.atom_slice(ndx[opt.center])
                        )[0]
                axis = opt.axis
            elif opt.axis:
                com = np.array([0, 0, 0])
                axis = opt.axis
            elif opt.moment and opt.extrem:
                com = md.compute_center_of_mass(
                        frame.atom_slice(ndx[opt.moment])
                        )[0]
                axis = find_axis(
                        frame.atom_slice(ndx[opt.moment]),
                        moment=opt.extrem,
                        margins=margins
                        )[0]
            else:
                raise Exception("Axis not defined!")
            r = []
            for k in range(opt.slices):
                rr = compute_optimal_rotation(
                        frame.atom_slice(
                            ndx[opt.group]).atom_slice(
                                grps[f"slice_{k+1}"]),
                        axis,
                        com,
                        ref=reference.atom_slice(
                            ndx[opt.group]).atom_slice(
                                grps[f"slice_{k+1}"]),
                        deg=deg)
                r.extend(rr)
            result.append(r)
            noise("done")

    np.savetxt(opt.out, np.array(result), fmt='%8.3f')


if __name__ == "__main__":
    main()
