

class Ndx(dict):
    """Dict-like object holding gromacs ndx file,
    numeration compatible with mdtraj selection
    (starting from 0, not 1)
    """

    def __init__(self, fnm=None, *args, **kwargs):
        super(Ndx, self).__init__(*args, **kwargs)
        if fnm is not None:
            self.readfile(fnm)

    def readfile(self, fnm):
        with open(fnm) as f:
            for line in f:
                if line.startswith("["):
                    key = line.strip("[").strip("\n").strip("]").strip()
                    self[key] = []
                else:
                    self[key].extend(line.strip(" \n").split())
        self.__renumerate()

    def writefile(self, fnm):
        with open(fnm, 'w') as f:
            for key, value in self.items():
                f.write(f"[ {key} ]\n")
                try:
                    for i in range(0, len(value), 15):
                        f.write(' '.join([f"{1+value[i+j]:4}" for j in range(15)]))
                        f.write('\n')
                except IndexError:
                    i = len(value) % 15
                    f.write(' '.join([f"{1+value[-i+j]:4}" for j in range(i)]))
                    f.write('\n')


    def __renumerate(self):
        for key in self.keys():
            for i in range(len(self[key])):
                self[key][i] = int(self[key][i]) - 1

    def __repr__(self):
        return f'<Ndx object with keys: {list(self.keys())}>'
