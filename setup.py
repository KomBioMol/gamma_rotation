from setuptools import setup, find_packages

setup(
        name="gamma_rotation",
        version="0.3",
        description="Small package to compute angle of gamma rotation in ATP \
synthase (and similar cases) using optimal rotation matrix.",
        author="Cyprian Kleist, Michał Badocha",
        author_email="Cyp2505@wp.pl",
        packages=find_packages(),
        scripts=["./bin/gamma_rotation"]
    )

