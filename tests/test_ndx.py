import unittest

from gamma_rotation.ndx import Ndx


class TestNdx(unittest.TestCase):

    def test_read(self):
        n = Ndx("tests/data/test_index.ndx")
        self.assertEqual(
                n["System"], list(range(0, 65623)), "Wrong ndx parsing")
        self.assertEqual(
                n["Protein"], list(range(0, 5985)), "Wrong ndx parsing")


if __name__ == "__main__":
    unittest.main()
