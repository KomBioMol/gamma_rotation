import unittest
import numpy as np
import mdtraj as md

from gamma_rotation.traj_tools import (
        find_marginal_atoms,
        slice_along_axis,
        find_axis,
        compute_optimal_rotation
        )


class Test_find_marginal_atoms(unittest.TestCase):

    def test_0(self):
        ref = md.load_pdb('tests/data/ex1.pdb')
        axis = np.array([0, 0, 1])
        result = (0, 4)
        np.testing.assert_array_equal(
                find_marginal_atoms(ref, axis),
                result,
                err_msg="Error in finding marginal atoms")


class Test_slice_along_axis(unittest.TestCase):

    def test_0(self):
        ref = md.load_pdb('tests/data/ex2.pdb')
        axis = np.array([0, 0, 1])
        d = {"slice_1": [0, 1, 2, 3, 4, 5, 6]}
        result = slice_along_axis(ref, axis, 1)
        for key, val in result.items():
            np.testing.assert_equal(
                    result[key],
                    d[key],
                    err_msg="Error in case with one slice")

    def test_1(self):
        ref = md.load_pdb('tests/data/ex2.pdb')
        axis = np.array([0, 0, 1])
        d = {f"slice_{i}": [7-i] for i in range(1,8)}
        result = slice_along_axis(ref, axis, 7)
        for key, val in result.items():
            np.testing.assert_equal(
                    result[key],
                    d[key],
                    err_msg="Error in case with multiple slices")


class Test_compute_optimal_rotation(unittest.TestCase):

    def test_0(self):
        traj = md.load_xtc('tests/data/ex1.xtc', top='tests/data/ex1.pdb')
        axis = np.array([0, 0, 1])
        point = np.array([0, 0, 0])
        result = np.array([0, -np.pi/4, -np.pi/2])
        np.testing.assert_array_almost_equal(
                compute_optimal_rotation(traj, axis, point),
                result,
                err_msg="Error in computing opptimal rotation for ex1")


class Test_find_axis(unittest.TestCase):

    def test_0(self):
        traj = md.load_xtc('tests/data/ex1.xtc', top='tests/data/ex1.pdb')
        result = np.array([[0, 0, 1], [0, 0, 1], [0, 0, 1]])
        np.testing.assert_array_almost_equal(
                find_axis(traj),
                result,
                err_msg="Error in finding axis for ex1")


if __name__ == "__main__":
    unittest.main()
