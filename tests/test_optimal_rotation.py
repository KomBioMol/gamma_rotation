import unittest
import numpy as np

from gamma_rotation.optimal_rotation import (
    rotate,
    translate,
    align_with_Oz,
    compute_optimal_angle_Oz,
    minimize_rmsd
    )


class Test_rotate(unittest.TestCase):

    def test_0(self):
        xyz = np.array([[1, 0, 0]])
        angle = np.pi
        axis = np.array([0, 0, 1])
        result = np.array([[-1, 0, 0]])
        np.testing.assert_array_almost_equal(
                rotate(xyz, angle, axis),
                result,
                err_msg="Error in simplest rotation")

    def test_1(self):
        xyz = np.array([[1, 0, 0]])
        angle = 180
        axis = np.array([0, 0, 1])
        result = np.array([[-1, 0, 0]])
        np.testing.assert_array_almost_equal(
                rotate(xyz, angle, axis, deg=True),
                result,
                err_msg="Error in degrees rotation")


class Test_translate(unittest.TestCase):

    def test_0(self):
        xyz = np.array([[1, 1, 1]])
        result = np.array([[0, 0, 0]])
        np.testing.assert_array_almost_equal(
                translate(xyz),
                result,
                err_msg="Error in CoG translation")

    def test_1(self):
        xyz = np.array([[1, 1, 1]])
        vec = np.array([1, 2, -3])
        result = np.array([[2, 3, -2]])
        np.testing.assert_array_almost_equal(
                translate(xyz, vec=vec),
                result,
                err_msg="Error in vector translation")


class Test_align_with_Oz(unittest.TestCase):

    def test_0(self):
        xyz = np.array([[0, 1, 1], [0, -1, 1], [0, -1, -1], [0, 1, -1]])
        point = np.array([0, 0, 0])
        vec = np.array([0, 0, 1])
        result = np.array([[0, 1, 1], [0, -1, 1], [0, -1, -1], [0, 1, -1]])
        np.testing.assert_array_almost_equal(
                align_with_Oz(xyz, point, vec),
                result,
                err_msg="Error in not moving case")

    def test_1(self):
        xyz = np.array([[0, 1, 1], [0, -1, 1], [0, -1, -1], [0, 1, -1]])
        point = np.array([0, 0, 0])
        vec = np.array([1, 0, 0])
        result = np.array([[-1, 1, 0], [-1, -1, 0], [1, -1, 0], [1, 1, 0]])
        np.testing.assert_array_almost_equal(
                align_with_Oz(xyz, point, vec),
                result,
                err_msg="Error in aligning vector")


class Test_compute_optimal_angle_Oz(unittest.TestCase):

    def test_0(self):
        ref = np.array([[1, 0, 0]])
        xyz = np.array([[0, 1, 0]])
        result = np.pi/2
        np.testing.assert_array_almost_equal(
                compute_optimal_angle_Oz(xyz, ref),
                result,
                err_msg="Error in calculating rotation in one-point case")

    def test_1(self):
        ref = np.array([[1, 1, 0], [-1, 1, 0], [-1, -1, 0], [1, -1, 0]])
        xyz = np.array([[1, -1, 0], [1, 1, 0], [-1, 1, 0], [-1, -1, 0]])
        result = -1/2*np.pi
        np.testing.assert_array_almost_equal(
                compute_optimal_angle_Oz(xyz, ref),
                result,
                err_msg="Error in calculating rotation in simplest case")

    def test_2(self):
        ref = np.array([[1, 2, 3], [3, 4, 5], [4, 5, 4], [7, 1, 6]])
        xyz = np.array([[2.232050, 0.133974, 3], [4.964101, -0.598076, 5], [6.330127, -0.964101, 4], [4.366025, -5.562177, 6]]) # rotated about 60 deg
        result = -1/3*np.pi
        np.testing.assert_array_almost_equal(
                compute_optimal_angle_Oz(xyz, ref),
                result,
                err_msg="Error in calculating rotation in more complex case")

    def test_3(self):   #rotation relative to CoG
        angle = np.pi/4
        ref = np.array([[-1, -1, 0], [-1, 1, 0], [1, -1, 0], [1, 1, 0]])
        test_rot = np.array([ [np.cos(angle), -np.sin(angle), 0], [np.sin(angle), np.cos(angle), 0], [0, 0, 1]])
        xyz = test_rot.dot(ref.T).T
        np.testing.assert_array_almost_equal(
                compute_optimal_angle_Oz(xyz, ref),
                angle,
                err_msg="Error in calculating rotation via method 1")

    def test_4(self):   #rotation relative to (0,0,0)
        angle = np.pi/4
        ref = np.array([[-1, -1, 0], [-1, 1, 0], [1, -1, 0], [1, 1, 0]])
        # test_rot = np.array([ [np.cos(angle), -np.sin(angle), 0], [np.sin(angle), np.cos(angle), 0], [0, 0, 1]])
        # xyz = test_rot.dot(ref.T).T
        xyz = np.array([[-1, -1, 0], [-1, 1, 0], [1, -1, 0], [1, 1, 0]])
        ref = translate(ref, vec=np.array([10,0,0]))
        xyz = translate(xyz, vec=np.array([0,10,0]))
        np.testing.assert_array_almost_equal(
                compute_optimal_angle_Oz(xyz, ref),
                minimize_rmsd(xyz, ref),
                err_msg="Error in calculating rotation via method 2")


class Test_minimize_rmsd(unittest.TestCase):

    def test_0(self):
        ref = np.array([[1, 0, 0]])
        xyz = np.array([[0, 1, 0]])
        result = np.pi/2
        np.testing.assert_array_almost_equal(
                minimize_rmsd(xyz, ref),
                result,
                err_msg="Error in calculating rotation in one-point case")

    def test_1(self):
        angle = np.pi/4
        ref = np.array([[-1, -1, 0], [-1, 1, 0], [1, -1, 0], [1, 1, 0]])
        # test_rot = np.array([ [np.cos(angle), -np.sin(angle), 0], [np.sin(angle), np.cos(angle), 0], [0, 0, 1]])
        # xyz = test_rot.dot(ref.T).T
        xyz = np.array([[-1, -1, 0], [-1, 1, 0], [1, -1, 0], [1, 1, 0]])
        ref = translate(ref, vec=np.array([10,0,0]))
        xyz = translate(xyz, vec=np.array([0,10,0]))
        np.testing.assert_array_almost_equal(
                minimize_rmsd(xyz, ref),
                np.pi/2,
                decimal=1,
                err_msg="Error in calculating rotation")


if __name__ == "__main__":
    unittest.main()
